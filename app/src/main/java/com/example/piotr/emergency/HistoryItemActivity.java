package com.example.piotr.emergency;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;


public class HistoryItemActivity extends AppCompatActivity
{
    private QuestionRepository questionRepository;
    private QuestionAnswerRepository questionAnswerRepository;
    private List<HistoryItem> historyItems = new ArrayList<HistoryItem>();
    TextView titleView;

    private History history;

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_item);
        listView = (ListView)findViewById(R.id.listView);

        this.questionRepository = new QuestionRepository(this);
        this.questionAnswerRepository = new QuestionAnswerRepository(this);
        titleView = (TextView)findViewById(R.id.title);
        Intent i = getIntent();
        history = (History)i.getSerializableExtra("myHistory");
        ShowHistoryTitle();
        Reload();
        PutToListView();
    }

    public void Reload()
    {
        List<QuestionAnswer> questionAnswerList = new ArrayList<QuestionAnswer>();
        questionAnswerList = questionAnswerRepository.getQuestionAnswerByHistoryId(history.getId());

        for(QuestionAnswer qa: questionAnswerList)
        {
            String question = questionRepository.getQuestionById(qa.getIdQuestion()).getQuestion() + "?";
            int answere = qa.getAnswer();

            HistoryItem historyItem = new HistoryItem(question, answere);
            historyItems.add(historyItem);
        }

    }

    public void ShowHistoryTitle()
    {
        String title = "My questionnaire\n" + Converter.dateToString(history.getDate());
        titleView.setText(title);

    }

    public void PutToListView()
    {
        ArrayAdapter<HistoryItem> historyItemAdapter= new ArrayAdapter<HistoryItem>(this, android.R.layout.simple_list_item_1, historyItems);
        listView.setAdapter(historyItemAdapter);
    }





}
