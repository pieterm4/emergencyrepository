package com.example.piotr.emergency;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by Piotr on 11-Nov-16.
 */

public class History implements Serializable
{
    private long id;
    private Date date;

    public History()
    {

    }

    public History(long _id, Date _date)
    {
        this.id = _id;

        this.date = _date;
    }

    //Getters

    public long getId()
    {
        return this.id;
    }


    public Date getDate()
    {
        return this.date;
    }

    //Setters
    public void setId(long _id)
    {
        this.id = _id;
    }


    public void setDate(Date _date)
    {
        this.date = _date;
    }


    public String toString()
    {
        return "Questionnaire: " + Converter.dateToString(date);
    }



}
