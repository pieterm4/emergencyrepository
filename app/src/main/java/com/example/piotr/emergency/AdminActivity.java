package com.example.piotr.emergency;

import android.database.SQLException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends AppCompatActivity
{
    private QuestionRepository questionRepository;
    private List<Question> questionList = new ArrayList<Question>();

    ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_question);

        this.questionRepository = new QuestionRepository(this);
        listview = (ListView)findViewById(R.id.listView);

        registerForContextMenu(listview);


        ReloadAndGetQuestions();


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_question, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
       AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId())
        {
            case R.id.deleteQuestion:
                Question question = (Question)listview.getItemAtPosition(info.position);
                //Deleting - rather changing the status :D
                questionRepository.ChangeStatus(question, 0);
                Toast.makeText(this, "Question has been deleted!", Toast.LENGTH_LONG).show();
                ReloadAndGetQuestions();
                return true;
        }

        return super.onContextItemSelected(item);
    }

    public void AddQuestionClick(View v)
    {
        EditText oEDT1 = (EditText)findViewById(R.id.questionEntry);
        String question = oEDT1.getText().toString();

        if(!TextUtils.isEmpty(question))
        {
            if(question.charAt(question.length()-1) == '?')
            {
                Toast.makeText(AdminActivity.this, "Please remove '?' from the end of question", Toast.LENGTH_LONG).show();
            }
            else
            {
                //Add to Database
                try
                {
                    Question createdquestion = questionRepository.createQuestion(question);
                    oEDT1.setText("");

                    // List<Question> questions = questionRepository.getAllQuestions();
                    Toast.makeText(this, "Question has been added!", Toast.LENGTH_LONG).show();
                    ReloadAndGetQuestions();

                }
                catch(SQLException e)
                {
                    Toast.makeText(this, "Something went wrong\n" + e.toString(), Toast.LENGTH_LONG).show();
                }
            }

        }
        else
        {
            Toast.makeText(AdminActivity.this, "You didn't write any question!", Toast.LENGTH_LONG).show();
        }
    }

    public void ReloadAndGetQuestions()
    {
        questionList.clear();
        List<Question> listq = new ArrayList<Question>(questionRepository.getAllQuestions());

        for(Question q: listq)
        {
            if(q.getStatus() == 1)
            {
                questionList.add(q);
            }
        }
        ArrayAdapter<Question> questionAdapter = new ArrayAdapter<Question>(this, android.R.layout.simple_list_item_1, questionList);
        questionAdapter.notifyDataSetChanged();
        listview.setAdapter(questionAdapter);
    }

}
