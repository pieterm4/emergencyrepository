package com.example.piotr.emergency;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Piotr on 15-Nov-16.
 */

public class EmailSending
{
    private String to;
    private String subject;
    private String message;

    private final Context context;

    public EmailSending(Context context)
    {
        this.context = context;
    }

    public EmailSending(Context context, String to, String subject, String message)
    {
        this.to = to;
        this.subject = subject;
        this.message = message;
        this.context = context;
    }

    public boolean sendEmail()
    {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, this.to);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, this.subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, this.message);

        try
        {
            context.startActivity(Intent.createChooser(emailIntent, "Choose an email client..."));
            return true;
        }
        catch (Exception ex)
        {
            Toast.makeText(context, "Email could't be send+\n", Toast.LENGTH_LONG).show();
        }
        return false;
    }


}
