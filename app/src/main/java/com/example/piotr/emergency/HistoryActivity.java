package com.example.piotr.emergency;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HistoryActivity extends AppCompatActivity
{

    private HistoryRepository historyRepository;
    private List<History> historyList = new ArrayList<History>();

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_view);

        this.historyRepository = new HistoryRepository(this);
        listView = (ListView)findViewById(R.id.listView);
        registerForContextMenu(listView);
        ReloadAndGetHistory();
    }

    public void ReloadAndGetHistory()
    {
        historyList.clear();
        historyList = historyRepository.getAllHistories();
        Collections.sort(historyList, Collections.reverseOrder(new HistoryComparator()));

        ArrayAdapter<History> historyAdapter = new ArrayAdapter<History>(this, android.R.layout.simple_list_item_1, historyList);
        listView.setAdapter(historyAdapter);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId())
        {
            case R.id.showQuestions:
                History history = (History)listView.getItemAtPosition(info.position);

                Intent i = new Intent(HistoryActivity.this, HistoryItemActivity.class);
                i.putExtra("myHistory", history);
                startActivity(i);
                //finish();
                return true;
        }


        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu_history, menu);
    }




}
