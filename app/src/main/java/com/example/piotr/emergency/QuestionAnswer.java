package com.example.piotr.emergency;

import java.util.Date;

/**
 * Created by Piotr on 11-Nov-16.
 */

public class QuestionAnswer
{
    private long id;
    private long idQuestion;
    private int answere;
    private long idHistory;

    public QuestionAnswer()
    {

    }

    public QuestionAnswer(long _id, long _idQuestion, int _answer, int _idHistory)
    {
        this.id = _id;
        this.idQuestion = _idQuestion;
        this.answere = _answer;
        this.idHistory = _idHistory;

    }

    //Getters
    public long getId()
    {
        return this.id;
    }
    public long getIdQuestion()
    {
        return this.idQuestion;
    }

    public int getAnswer()
    {
        return this.answere;
    }

    public long getIdHistory()
    {
        return this.idHistory;
    }


    //Setters
    public void setId(long _id)
    {
        this.id = _id;
    }
    public void setIdQuestion(long _id)
    {
        this.idQuestion = _id;
    }

    public void setAnswere(int _answere)
    {
        this.answere = _answere;
    }

    public void setIdHistory(long _idHistory)
    {
        this.idHistory = _idHistory;
    }



}
