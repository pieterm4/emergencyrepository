package com.example.piotr.emergency;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuestionAnswereActivity extends AppCompatActivity
{
    private static RadioGroup radioGroup;
    private static RadioButton radioButton;
    private static TextView textView;
    private static int countQuestion;
    private int counter = 0;

    private Question question = new Question();
    private QuestionAnswerRepository questionAnswerRepository;
    private HistoryRepository historyRepository;

    private QuestionRepository questionRepository;
    private List<Question> questionList = new ArrayList<Question>();

    private List<QuestionAnswer> answeredQuestions = new ArrayList<QuestionAnswer>();

    private List<HistoryItem> historyItems = new ArrayList<HistoryItem>();

    private long lastHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_answere);

        radioGroup = (RadioGroup)findViewById(R.id.radiobuttonGroup);
        textView = (TextView)findViewById(R.id.question);
        this.questionRepository = new QuestionRepository(this);
        this.questionAnswerRepository = new QuestionAnswerRepository(this);
        this.historyRepository = new HistoryRepository(this);

        ReloadAndGetQuestions();
        countQuestion = questionList.size();


        setQuestion(counter);
        lastHistory  = getLastHistoryId();

    }

    public void ReloadAndGetQuestions()
    {
        questionList.clear();
        List<Question> questions = new ArrayList<Question>(questionRepository.getAllQuestions());

        for(Question q :questions)
        {
            if(q.getStatus() == 1)
            {
                questionList.add(q);
            }
        }

    }

    public void nextClick(View v)
    {


        int checked = radioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton)findViewById(checked);

        int answere;
        String odp = radioButton.getText().toString();

        if(counter == countQuestion -1)
        {
            if(odp.equals("Yes"))
            {
                answere = 1;
                QuestionAnswer newQuestionAnswere = setAnswer(question, answere);

                newQuestionAnswere.setIdHistory(lastHistory + 1);
                answeredQuestions.add(newQuestionAnswere);

                addToDatabase();


                textView.setText("Thank you!\nQuestionaire has finished");
                finish();
                //GotoHistory();

            }
            if (odp.equals("No"))
            {
                answere = 0;
                QuestionAnswer newQuestionAnswere = setAnswer(question, answere);

                newQuestionAnswere.setIdHistory(lastHistory +  1);
                answeredQuestions.add(newQuestionAnswere);

                addToDatabase();


                textView.setText("Thank you!\nQuestionaire has finished");
                finish();
                //GotoHistory();

            }


        }

        if(counter < countQuestion - 1)
        {

            if(odp.equals("Yes"))
            {
                answere = 1;
                QuestionAnswer newQuestionAnswere = setAnswer(question, answere);

                newQuestionAnswere.setIdHistory(lastHistory + 1);
                answeredQuestions.add(newQuestionAnswere);


            }
            if (odp.equals("No"))
            {
                answere = 0;
                QuestionAnswer newQuestionAnswere = setAnswer(question, answere);

                newQuestionAnswere.setIdHistory(lastHistory +  1);
                answeredQuestions.add(newQuestionAnswere);


            }

            setQuestion(++counter);
        }

    }

    private void setQuestion(int counter)
    {
        question = questionList.get(counter);

        textView.setText(question.getQuestion() + "?");
    }

    private QuestionAnswer setAnswer(Question question, int answere)
    {
        HistoryItem historyItem = new HistoryItem();
        QuestionAnswer qa = new QuestionAnswer();
        if(question != null)
        {
            qa.setIdQuestion(question.getId());
            qa.setAnswere(answere);

            //historyItem
            historyItem.setQuestion(question.getQuestion());
            historyItem.setAnswere(answere);
            historyItems.add(historyItem);

            return qa;
        }
        else
        {
            return null;
        }

    }

    private long getLastHistoryId()
    {
        List<History> historyList = historyRepository.getAllHistories();
        if(historyList.isEmpty())
        {
            return 0;
        }
        else
        {
            long lastId = historyList.get(historyList.size()-1).getId();
            return lastId;
        }

    }

    private void addToDatabase()
    {
        Date date = new Date();
        History history = new History();

        history = historyRepository.createHistory(date);

        for(QuestionAnswer qw: answeredQuestions)
        {
            questionAnswerRepository.insertQuestionAnswere(qw);
        }

        saveToFile(date);
        senMail(date);
        answeredQuestions.clear();


    }


    public void senMail(Date date)
    {
        String message ="My questionnaire: " + Converter.dateToString(date) + "\n\n";

        for(HistoryItem hi:historyItems)
        {
            message += hi.toString() + "\n\n";
        }

        EmailSending sendMail = new EmailSending(this, "piotrmakowiec@outlook.com", "Emergency App", message);
        sendMail.sendEmail();

    }

    public void saveToFile(Date date)
    {
        String message ="My questionnaire: " + Converter.dateToString(date) + "\n\n";

        for(HistoryItem hi:historyItems)
        {
            message += hi.toString() + "\n\n";
        }

        SaveToFile.Save("My_questionnaire"+Converter.dateToStringFileFormat(date)+".txt", message, this);

    }



}
