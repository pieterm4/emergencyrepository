package com.example.piotr.emergency;

import java.util.Comparator;

/**
 * Created by Piotr on 15-Nov-16.
 */

public class HistoryComparator implements Comparator<History>
{

    @Override
    public int compare(History o1, History o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}
