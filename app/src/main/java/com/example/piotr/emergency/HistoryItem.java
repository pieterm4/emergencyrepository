package com.example.piotr.emergency;

/**
 * Created by Piotr on 15-Nov-16.
 */

public class HistoryItem
{
    private String question;
    private String answere;

    public HistoryItem()
    {

    }

    public HistoryItem(String question, int answere)
    {
        this.question = question;

        if(answere == 0)
        {
            this.answere = "No";
        }
        if(answere == 1)
        {
            this.answere = "Yes";
        }
    }

    //Getters
    public String getQuestion()
    {
        return this.question;
    }

    public String getAnswere()
    {
        return this.answere;
    }

    //Setters

    public void setQuestion(String _question)
    {
        this.question = _question;
    }

    public void setAnswere(int _answere)
    {
        if(_answere == 0)
        {
            this.answere = "No";
        }
        if(_answere == 1)
        {
            this.answere = "Yes";
        }
    }

    public String toString()
    {
        return this.question + "\n" + this.answere;
    }
}
