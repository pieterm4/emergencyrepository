package com.example.piotr.emergency;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Piotr on 12-Nov-16.
 */

public class QuestionRepository
{
    //Database fields

    private SQLiteDatabase mDatabase;
    private DatabaseHelper mDbHelper;
    private Context mContext;
    private String[] mAllColumns = {DatabaseHelper.QUESTION_ID, DatabaseHelper.COLUMN_QUESTION, DatabaseHelper.COLUMN_QUESTION_STATUS};

    public QuestionRepository(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(context);

        //Open the database

        try
        {
            open();
        }
        catch(SQLException e)
        {
            Log.e(TAG, "SQLException on openning database " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void open() throws SQLException
    {
        mDatabase = mDbHelper.getWritableDatabase();
    }
    public void close()
    {
        mDbHelper.close();
    }

    public Question createQuestion(String _question)
    {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_QUESTION, _question);
        values.put(DatabaseHelper.COLUMN_QUESTION_STATUS, 1);

        long insertId = mDatabase.insert(DatabaseHelper.TABLE_QUESTION, null, values);

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTION, mAllColumns, DatabaseHelper.QUESTION_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Question newQuestion = cursorToQuestion(cursor);
        cursor.close();

        return newQuestion;
    }

    protected Question cursorToQuestion(Cursor cursor)
    {
        Question question = new Question();
        if(cursor != null)
        {

            question.setId(cursor.getLong(0));
            question.setQuestion(cursor.getString(1));
            question.setStatus(cursor.getInt(2));
        }


        return question;
    }

    public void deleteQuestion(Question question)
    {
        long id = question.getId();
        //To implement

        QuestionAnswerRepository questionAnswerRepository = new QuestionAnswerRepository(mContext);
        List<QuestionAnswer> listQuestionAnswers = questionAnswerRepository.getQuestionAnswerByQuestionId(id);

        if(listQuestionAnswers != null && !listQuestionAnswers.isEmpty())
        {
            for(QuestionAnswer q: listQuestionAnswers)
            {
                questionAnswerRepository.deleteQuestionAnswer(q);
            }
        }


        mDatabase.delete(DatabaseHelper.TABLE_QUESTION, DatabaseHelper.QUESTION_ID + " = " + id, null);

    }

    public List<Question> getAllQuestions()
    {
        List<Question> listQuestion = new ArrayList<Question>();

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTION, mAllColumns, null, null, null, null, null);

        if(cursor != null)
        {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                Question question = cursorToQuestion(cursor);
                listQuestion.add(question);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return listQuestion;
    }

    public Question getQuestionById(long id)
    {
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTION, mAllColumns, DatabaseHelper.QUESTION_ID + " = ?", new String[]{String.valueOf(id)}, null, null, null );
        if(cursor != null)
        {
            cursor.moveToFirst();
        }

        Question question = cursorToQuestion(cursor);
        return question;
    }

    public void ChangeStatus(Question question, int status)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.COLUMN_QUESTION, question.getQuestion());
        values.put(DatabaseHelper.COLUMN_QUESTION_STATUS, status);

        long updateId = mDatabase.update(DatabaseHelper.TABLE_QUESTION, values, DatabaseHelper.QUESTION_ID + " = " + question.getId(), null);


    }
}
