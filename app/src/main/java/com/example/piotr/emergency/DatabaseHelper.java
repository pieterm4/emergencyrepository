package com.example.piotr.emergency;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Piotr on 11-Nov-16.
 */

public class DatabaseHelper extends SQLiteOpenHelper
{
    public static final String TAG = "DBHelper";

    public static final String DATABASE_NAME = "questionnaire.sqllite";
    public static final int DATABASE_VERSION = 1;

    //Columns of the Question Table
    public static final String TABLE_QUESTION = "question";
    public static final String QUESTION_ID = "id";
    public static final String COLUMN_QUESTION = "question";
    public static final String COLUMN_QUESTION_STATUS = "status";

    //Columns of QuestionAnswer Table
    public static final String TABLE_QUESTIONANSWER = "questionAnswere";
    public static final String COLUMN_QUESTIONANSWER_ID = "id";
    public static final String COLUMN_QUESTIONANSWER_QUESTIONID = "questionId";
    public static final String COLUMN_QUESTIONANSWER_ANSWER = "answere";
    public static final String COLUMN_QUESTIONANSWER_HISTORYID = "idHistory";

    //Columns of History Table
    public static final String TABLE_HISTORY = "history";
    public static final String COLUMN_HISTORY_ID = "id";
    public static final String COLUMN_HISTORY_DATE="date";


    public static final String SLQ_CREATE_QUESTION_TABLE = "CREATE TABLE " + TABLE_QUESTION + "("
            + QUESTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_QUESTION + " VARCHAR(50) NOT NULL, "
            + COLUMN_QUESTION_STATUS + " INTEGER NOT NULL "
            + " );";

    public static final String SQL_CREATE_QUESTION_ANSWER_TABLE = "CREATE TABLE " + TABLE_QUESTIONANSWER + "("
            + COLUMN_QUESTIONANSWER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_QUESTIONANSWER_QUESTIONID + " INTEGER NOT NULL, "
            + COLUMN_QUESTIONANSWER_ANSWER + " INTEGER NOT NULL, "
            + COLUMN_QUESTIONANSWER_HISTORYID + " INTEGER NOT NULL "
            + " );";

    public static final String SQL_CREATE_HISTORY = "CREATE TABLE " + TABLE_HISTORY + "("
            + COLUMN_HISTORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_HISTORY_DATE + " VARCHAR(50) NOT NULL "
            + " );";

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(SLQ_CREATE_QUESTION_TABLE);
        db.execSQL(SQL_CREATE_HISTORY);
        db.execSQL(SQL_CREATE_QUESTION_ANSWER_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);

        //Clear all data
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONANSWER);

        //Recreate tables
        onCreate(db);


    }
}
