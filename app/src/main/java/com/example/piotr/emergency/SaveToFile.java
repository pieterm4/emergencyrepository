package com.example.piotr.emergency;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static android.content.ContentValues.TAG;

/**
 * Created by Piotr on 16-Nov-16.
 */

public class SaveToFile
{

    public static void Save(String filename, String message, Context context)
    {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {

                try
                {
                    File root = new File(Environment.getExternalStorageDirectory(), "Emergency Saved Data");
                    if (!root.exists())
                    {
                        root.mkdirs();
                    }

                    File gpxfile = new File(root, filename);
                    FileWriter writer = new FileWriter(gpxfile);
                    writer.append(message);
                    writer.flush();
                    writer.close();
                    Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
                }
                catch (IOException e)
                {
                    Log.v(TAG, e.getMessage());
                    Toast.makeText(context, "Not saved", Toast.LENGTH_SHORT).show();
                }
        }
    }


}
