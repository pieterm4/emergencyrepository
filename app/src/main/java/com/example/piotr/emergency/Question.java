package com.example.piotr.emergency;

/**
 * Created by Piotr on 08-Nov-16.
 */

public class Question
{

    private long id;
    private String question;
    private int status;

    public Question()
    {

    }

    public Question(int _id, String _question, int status)
    {
        this.id = _id;
        this.question = _question;
        this.status = status;
    }
    //Gettery
    public long getId()
    {
        return this.id;
    }

    public String getQuestion()
    {
        return this.question;
    }

    public int getStatus()
    {
        return this.status;
    }



    //Setery
    public void setId(long ID)
    {
        this.id = ID;
    }
    public void setQuestion(String Question)
    {
        this.question = Question;
    }

    public  void setStatus(int status)
    {
        this.status = status;
    }

    public String toString()
    {
        return question + "?";
    }


}
