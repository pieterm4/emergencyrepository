package com.example.piotr.emergency;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Piotr on 12-Nov-16.
 */

public class QuestionAnswerRepository
{
    private SQLiteDatabase mDatabase;
    private DatabaseHelper mDbHelper;
    private Context mContext;
    private String[] mAllColumns = {DatabaseHelper.COLUMN_QUESTIONANSWER_ID, DatabaseHelper.COLUMN_QUESTIONANSWER_QUESTIONID, DatabaseHelper.COLUMN_QUESTIONANSWER_ANSWER, DatabaseHelper.COLUMN_QUESTIONANSWER_HISTORYID};

    public QuestionAnswerRepository(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(context);

        //Open the database

        try
        {
            open();
        }
        catch(SQLException e)
        {
            Log.e(TAG, "SQLException on openning database " + e.getMessage());
            e.printStackTrace();
        }
    }
    public void close()
    {
        mDbHelper.close();
    }

    public void open() throws SQLException
    {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public QuestionAnswer createQuestionQnswere(int _answere, long _questionId, long _historyId)
    {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_QUESTIONANSWER_QUESTIONID, _questionId);
        values.put(DatabaseHelper.COLUMN_QUESTIONANSWER_ANSWER, _answere);
        values.put(DatabaseHelper.COLUMN_QUESTIONANSWER_HISTORYID, _historyId);

        long insertId = mDatabase.insert(DatabaseHelper.TABLE_QUESTIONANSWER, null, values);

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTIONANSWER, mAllColumns, DatabaseHelper.COLUMN_QUESTIONANSWER_ID + " = " + insertId, null, null, null, null);

        cursor.moveToFirst();
        QuestionAnswer newQuestionAnswer = cursorToQuestionAnswer(cursor);
        cursor.close();

        return newQuestionAnswer;

    }

    public void insertQuestionAnswere(QuestionAnswer questionAnswer)
    {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_QUESTIONANSWER_QUESTIONID, questionAnswer.getIdQuestion());
        values.put(DatabaseHelper.COLUMN_QUESTIONANSWER_ANSWER, questionAnswer.getAnswer());
        values.put(DatabaseHelper.COLUMN_QUESTIONANSWER_HISTORYID, questionAnswer.getIdHistory());

        long insertId = mDatabase.insert(DatabaseHelper.TABLE_QUESTIONANSWER, null, values);

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTIONANSWER, mAllColumns, DatabaseHelper.COLUMN_QUESTIONANSWER_ID + " = " + insertId, null, null, null, null);

        cursor.moveToFirst();
        QuestionAnswer newQuestionAnswer = cursorToQuestionAnswer(cursor);
        cursor.close();
    }

    public void deleteQuestionAnswer(QuestionAnswer _questionAnswer)
    {
        long id = _questionAnswer.getId();
        long historyId = _questionAnswer.getIdHistory();

        HistoryRepository historyRepository = new HistoryRepository(mContext);
        List<History> listHistory = historyRepository.getAllHistoryWithQuestionAnswereHistoryId(historyId);

        if(listHistory != null && !listHistory.isEmpty())
        {
            for(History h: listHistory)
            {
                historyRepository.deleteHistory(h);
            }
        }



        mDatabase.delete(DatabaseHelper.TABLE_QUESTIONANSWER, DatabaseHelper.COLUMN_QUESTIONANSWER_ID + " = " + id, null);

    }

    protected QuestionAnswer cursorToQuestionAnswer(Cursor cursor)
    {
        QuestionAnswer questionAnswer = new QuestionAnswer();
        if(cursor != null)
        {
            questionAnswer.setId(cursor.getLong(0));
            questionAnswer.setIdQuestion(cursor.getLong(1));
            questionAnswer.setAnswere(cursor.getInt(2));
            questionAnswer.setIdHistory(cursor.getLong(3));

        }

        return questionAnswer;
    }

    public List<QuestionAnswer> getQuestionAnswerByQuestionId(long questionId)
    {
        List<QuestionAnswer> questionAnswerList = new ArrayList<QuestionAnswer>();

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTIONANSWER, mAllColumns, DatabaseHelper.COLUMN_QUESTIONANSWER_ID + " = ?", new String[]{String.valueOf(questionId)}, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            QuestionAnswer questionAnswer = cursorToQuestionAnswer(cursor);
            questionAnswerList.add(questionAnswer);
            cursor.moveToNext();
        }

        cursor.close();
        return questionAnswerList;
    }

    public List<QuestionAnswer> getQuestionAnswerByHistoryId(long historyId)
    {
        List<QuestionAnswer> questionAnswerList = new ArrayList<QuestionAnswer>();
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTIONANSWER, mAllColumns, DatabaseHelper.COLUMN_QUESTIONANSWER_HISTORYID + " = ?", new String[]{String.valueOf(historyId)}, null, null, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast())
        {
            QuestionAnswer questionAnswer = cursorToQuestionAnswer(cursor);
            questionAnswerList.add(questionAnswer);
            cursor.moveToNext();
        }
        cursor.close();
        return questionAnswerList;

    }

    public List<QuestionAnswer> getAllQuestionAnswers()
    {
        List<QuestionAnswer> questionAnswerList = new ArrayList<QuestionAnswer>();

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTIONANSWER, mAllColumns, null, null, null, null, null);

        if(cursor != null)
        {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                QuestionAnswer questionAnswer = cursorToQuestionAnswer(cursor);
                questionAnswerList.add(questionAnswer);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return questionAnswerList;
    }

    public QuestionAnswer getQuestionAnswereById(long id)
    {
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_QUESTIONANSWER, mAllColumns, DatabaseHelper.COLUMN_QUESTIONANSWER_ID + " = ?", new String[]{String.valueOf(id)}, null, null, null );
        if(cursor != null)
        {
            cursor.moveToFirst();
        }

        QuestionAnswer questionAnswere = cursorToQuestionAnswer(cursor);
        return questionAnswere;
    }




}
