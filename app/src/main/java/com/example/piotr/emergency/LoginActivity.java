package com.example.piotr.emergency;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity
{
    private EditText loginText;
    private EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        loginText = (EditText)findViewById(R.id.loginEditText);
        passwordText = (EditText)findViewById(R.id.passwordEditText);

    }

    public void logInClick(View v)
    {
        String login = loginText.getText().toString();
        String password = passwordText.getText().toString();

        if(login.equals("admin") && password.equals("1234"))
        {
            Intent i = new Intent(this, AdminActivity.class);
            startActivity(i);
            finish();
        }
        else
        {
            Toast.makeText(this, "Incorrect login or password", Toast.LENGTH_LONG).show();
        }
    }
}
