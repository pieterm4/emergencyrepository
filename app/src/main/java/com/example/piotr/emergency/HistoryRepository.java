package com.example.piotr.emergency;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Piotr on 14-Nov-16.
 */

public class HistoryRepository
{
    private SQLiteDatabase mDatabase;
    private DatabaseHelper mDbHelper;
    private Context mContext;
    private String[] mAllColumns = {DatabaseHelper.COLUMN_HISTORY_ID, DatabaseHelper.COLUMN_HISTORY_DATE};

    public HistoryRepository(Context context)
    {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(context);

        //Open the database

        try
        {
            open();
        }
        catch(SQLException e)
        {
            Log.e(TAG, "SQLException on openning database " + e.getMessage());
            e.printStackTrace();
        }
    }

    public History createHistory(Date date)
    {

        ContentValues values = new ContentValues();

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String myData = df.format(date);
        values.put(DatabaseHelper.COLUMN_HISTORY_DATE, myData);

        long insertId = mDatabase.insert(DatabaseHelper.TABLE_HISTORY, null, values);

        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_HISTORY, mAllColumns, DatabaseHelper.COLUMN_HISTORY_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        History newHistory = cursorToHistory(cursor);
        cursor.close();

        return newHistory;


    }

    protected History cursorToHistory(Cursor cursor)
    {
        History history = new History();


        if(cursor != null)
        {
            history.setId(cursor.getLong(0));
            String date = cursor.getString(1);
            Date dateDate = Converter.stringToDate(date);

            history.setDate(dateDate);
        }

        return history;

    }



    public void open() throws SQLException
    {
        mDatabase = mDbHelper.getWritableDatabase();
    }
    public void close()
    {
        mDbHelper.close();
    }

    public void deleteHistory(History history)
    {
        long id = history.getId();

        QuestionAnswerRepository questionAnswerRepository = new QuestionAnswerRepository(mContext);
        List<QuestionAnswer> listQuestionAnswers = questionAnswerRepository.getQuestionAnswerByHistoryId(id);

        if(listQuestionAnswers != null && !listQuestionAnswers.isEmpty())
        {
            for(QuestionAnswer q: listQuestionAnswers)
            {
                questionAnswerRepository.deleteQuestionAnswer(q);
            }
        }

        System.out.println("The deleted history element has the id: " + id);
        mDatabase.delete(DatabaseHelper.TABLE_HISTORY, DatabaseHelper.COLUMN_HISTORY_ID + " = " + id, null);

    }

    public List<History> getAllHistories()
    {
        List<History> historyList = new ArrayList<History>();
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_HISTORY, mAllColumns, null, null, null, null, null);

        if(cursor != null)
        {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                History history = cursorToHistory(cursor);
                historyList.add(history);
                cursor.moveToNext();
            }

            cursor.close();
        }

        return historyList;
    }

    public History getHistoryById(long id)
    {
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_HISTORY, mAllColumns, DatabaseHelper.COLUMN_HISTORY_ID+ " = ?", new String[]{String.valueOf(id)}, null, null, null );
        if(cursor != null)
        {
            cursor.moveToFirst();
        }

        History history = cursorToHistory(cursor);
        return history;
    }

    public List<History> getAllHistoryWithQuestionAnswereHistoryId(long id)
    {
        List<History> historyList = new ArrayList<History>();
        Cursor cursor = mDatabase.query(DatabaseHelper.TABLE_HISTORY, mAllColumns, DatabaseHelper.COLUMN_HISTORY_ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast())
        {
            History history = cursorToHistory(cursor);
            historyList.add(history);
            cursor.moveToNext();
        }

        cursor.close();
        return historyList;
    }

}
